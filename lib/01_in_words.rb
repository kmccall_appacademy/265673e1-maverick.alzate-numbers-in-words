class Fixnum
  ONES = { 1 => "one",
           2 => "two",
           3 => "three",
           4 => "four",
           5 => "five",
           6 => "six",
           7 => "seven",
           8 => "eight",
           9 => "nine" }.freeze

  TEENS = { 0 => "ten",
            1 => "eleven",
            2 => "twelve",
            3 => "thirteen",
            4 => "fourteen",
            5 => "fifteen",
            6 => "sixteen",
            7 => "seventeen",
            8 => "eighteen",
            9 => "nineteen" }.freeze

  TENS = { 2 => "twenty",
           3 => "thirty",
           4 => "forty",
           5 => "fifty",
           6 => "sixty",
           7 => "seventy",
           8 => "eighty",
           9 => "ninety" }.freeze

  HUNDREDS = { 1 => "one hundred",
               2 => "two hundred",
               3 => "three hundred",
               4 => "four hundred",
               5 => "five hundred",
               6 => "six hundred",
               7 => "seven hundred",
               8 => "eight hundred",
               9 => "nine hundred", }.freeze


  WORD_ARRAY = [HUNDREDS, TENS, TEENS, ONES].freeze

  TRIAD_MARKERS = { 2 => "thousand",
                    3 => "million",
                    4 => "billion",
                    5 => "trillion" }.freeze

  def in_words
    words = []

    # if zero, just return zero
    return "zero" if self == 0

    # check length of number
    iter = self.to_s.length % 3 == 0 ? self.to_s.length / 3 : self.to_s.length / 3 + 1

    # loop through place triads
    while iter > 0
      # boolean helps us keep track of placeholder zeroes
      # e.g., in 10_000_001, triad #2 will have no conversion &
      # thus the program will not write "ten million thousand one"
      conversion = false
      # set booleans for current digits triad
      # dp stands for "digits place"
      dp = (-3..-1).map do |num|
        if self.to_s[num + -3 * (iter - 1)].to_i == 0 ||
          self.to_s[num + -3 * (iter - 1)] == nil
          false
        else
          true
        end
      end
      # hundreds
      if dp[0]
        dp[1], dp[2] = in_words_hundreds(dp[1], dp[2], words, iter)
        conversion = true
      end
      # tens
      if dp[1]
        dp[2] = in_words_tens(dp[2], words, iter)
        conversion = true
      end
      # ones
      if dp[2]
        in_words_ones(words, iter)
        conversion = true
      end
      # triad marker
      if iter > 1 && conversion
        words << TRIAD_MARKERS[iter]

      end
      iter -= 1
    end
    # return self as word string
    words.join(" ")
  end

  def in_words_hundreds(tens, ones, words, iter)
    # add word
    words << WORD_ARRAY[0][self.to_s[-3 + -3 * (iter - 1)].to_i]
    # if tens == 0
    if self.to_s[-2 + -3 * (iter - 1)].to_i == 0
      tens = false
    end
    # if ones == 0
    if self.to_s[-1 + -3 * (iter - 1)].to_i == 0
      ones = false
    end
    # return booleans
    [tens, ones]
  end

  def in_words_tens(ones, words, iter)
    # teens
    if self.to_s[-2 + -3 * (iter - 1)].to_i == 1
      words << WORD_ARRAY[2][self.to_s[-1 + -3 * (iter - 1)].to_i]
      ones = false
    # tens divisible by ten
    elsif self.to_s[-2 + -3 * (iter - 1)].to_i > 1 &&
      self.to_s[-1 + -3 * (iter - 1)].to_i == 0
      words << WORD_ARRAY[1][self.to_s[-2 + -3 * (iter - 1)].to_i]
      ones = false
    # tens not multiple of ten
    elsif self.to_s[-2 + -3 * (iter - 1)].to_i > 1 &&
      self.to_s[-1 + -3 * (iter - 1)].to_i > 0
      words << WORD_ARRAY[1][self.to_s[-2 + -3 * (iter - 1)].to_i]
    end
    # return boolean
    ones
  end

  def in_words_ones(words, iter)
    words << WORD_ARRAY[3][self.to_s[-1 + -3 * (iter - 1)].to_i]
  end
end
